#ifndef _CONTENT_H_
#define _CONTENT_H_

#include <string.h>

using namespace std;

static string CONTENT[] = { "Texas Instruments was founded in 1930 and is headquartered in Dallas, Texas",
                            "TI produced world's first commercial silicon transistor in 1954 and the same year designed and manufactured the first transistor radio",
                            "What do you call a person who accidentally plugged a 5V to 3.3V UART port? A serial killer",
                            "Jack Kilby invented the Integrated Circuits in 1958 while working in TI's Central Research Labs",
                            "Why do programmers confuse Christmas with Halloween? Because OCT 31 = DEC 25",
                            "How many software developers does it take to change a lightbulb? None, its a hardware problem",
                            "TI invented the hand held calculator in 1967",
                            "Why do programmers like dark mode? Because light attract bugs",
                            "TI introduced first single chip microcontroller in 1970",
                            "TI invented the DLP (Digital Light Processing) chip",
                            "What do you call a part time conductor? A semiconductor",
                            "TI was originally named General Instruments but later renamed to Texas Instruments as a firm named General Instruments alread existed",
                            "Jack Kilby won the Novel Prize in Physics for his part of the invention on integrated circuits",
                            "In 1979 TI entered the home computer market with the TI-99/4 but by 1983 it discontinued TI-99/4 due to internse price wars",
                            "TI is the largets producer of analog semiconductors and digital signal processors",
                            "What are clouds generally made of? Linux servers mostly.",
                            "EdgeAI is the implementation of artificial intelligence on edge computing environment",
                            "Texas Instruments was founded by Cecil H Green, J Erik Jonsson, Eugene McDermott, and Patric E Haggerty",
                            "What did the integrated circuit say when it was enlightened? IC",
                        };


#endif /* _CONTENT_H_ */